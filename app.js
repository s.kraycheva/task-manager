import express from "express";
import { getAllTasks } from "./controllers/tasks.js";
import router from "./routes/tasks.js";

const app = express();


const port = 3000; 

//routes

app.get('/hello', (req, res) => {
    res.send('Task-manager app')
})


app.listen(port, console.log(`Server is listening on ${port}`))

app.use('/api/v1/tasks',getAllTasks)